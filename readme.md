# Prueba tecnica para aspirante cargo desarrollador

Este es el resultado de la prueba técnica está dirigida a las personas que aspiran al cargo de desarrollador.

### 1. Copia del código fuente y scripts
Para el uso del resultado de la prueba técnica se debe clonar el proyecto en algún servidor web con PHP con conexión a MySQL.

### 2. Creación base de datos MySQL
Se debe crear una base de datos en MySQL con el nombre **usr_sisintpro** y que se conecte a MySQL con el usuario root y con contraseña toor. Para cambiar la configuración de conexión a la base de datos se debe cambiar las siguientes líneas en el archivo **.env**.

```php
    DB_DATABASE=usr_sisintpro
    DB_USERNAME=root
    DB_PASSWORD=toor
```

La base de datos MySQL con la información que se muestra en la aplicación se debe cargar a partir de la ejecución del siguiente comando **php artisan migrate:refresh --seed**, el cual crea las tablas y inserta los datos de prueba.

### 3. Ejecución de la aplicación
Como en mi caso trabaje en un servidor local [Laragon](https://laragon.org) y si el evaluador revisa el resultado de la prueba, puede ingresar al proyecto desde el siguiente enlace [http://personeria-test.test](http://personeria-test.test) (Para quitar el index.php de la URL se debe configurar el htaccess o archivo de configuración de su servidor web).
