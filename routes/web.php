<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'InterviewsController@index')->name('entrevistas.index');
Route::get('entrevistas', 'InterviewsController@index')->name('entrevistas.index');
Route::get('entrevistas/list', 'InterviewsController@list')->name('entrevistas.list');
Route::get('entrevistas/ver/{id}', 'InterviewsController@show')->name('entrevistas.ver')->where('id', '[0-9]+');
Route::get('entrevistas/editar/{id}', 'InterviewsController@edit')->name('entrevistas.editar')->where('id', '[0-9]+');
Route::get('entrevistas/borrar/{id}', 'InterviewsController@destroy')->name('entrevistas.borrar')->where('id', '[0-9]+');
