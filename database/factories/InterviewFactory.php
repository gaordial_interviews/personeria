<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Interview;
use Faker\Generator as Faker;
// use Carbon\Carbon;

$factory->define(Interview::class, function (Faker $faker) {
    return [
        'identificacion' => random_int(1,20),
        'nombres' => $faker->sentence(1),
        'apellidos' => $faker->sentence(1),
        'fec_nacimiento' => $faker->dateTimeBetween('-20 years', '+0 days'),
        'domicilio'  => $faker->sentence(1),
        'ciudad'  => $faker->sentence(1),
        'estado'  => '1'
    ];
});
