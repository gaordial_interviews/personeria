<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interviews', function (Blueprint $table) {
            $table->increments('id')->comment('ID de la entrevista');
            $table->integer('identificacion')->nullable()->comment('Identificacion del entrevistado');
            $table->string('nombres', 50)->nullable()->comment('Nombres del entrevistado');
            $table->string('apellidos', 50)->nullable()->comment('Apellidos del entrevistado');
            $table->date('fec_nacimiento', 100)->nullable()->comment('Fecha de nacimiento del entrevistado');
            $table->string('domicilio', 100)->nullable();
            $table->string('ciudad', 100)->nullable();
            $table->smallInteger('estado')->comment('Estado de la entrevista. 1-Activo, 2-Inactivo');
            $table->timestamp('created_at')->comment('Fecha de creación del registro');
            $table->timestamp('updated_at')->nullable()->comment('Fecha de última actualización del registro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interviews');
    }
}
