<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$interviews = factory(App\Interview::class, 20)->create();

		DB::table('interviews')->insert([
            'identificacion' => '80772099',
            'nombres' => 'Orlando',
            'apellidos' => 'Garzon',
            'fec_nacimiento' => '1985-03-14',
			'domicilio'  => 'Calle falsa 123',
			'ciudad'  => 'Bogota',
			'estado'  => '1',
			'created_at' => Carbon::now(),
		]);
	}
}
