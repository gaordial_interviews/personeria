<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $table = 'interviews';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	//protected $fillable = ['names', 'surnames', 'email', 'telephone', 'state'];

	/**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /*public function responsable()
    {
        return $this->belongsTo(Responsable::class);
    }*/
}
