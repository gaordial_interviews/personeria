<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.head')
</head>
<body>
    <div id="app">
        @include('partials.navbar')
        <main class="container">
            @yield('content')
        </main>
        @include('partials.footer')
    </div>
    <!-- Scripts -->
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ url('vendor/dataTables/jquery.dataTables.min.css') }}"></script>

	@isset ($arrJs)
	    @foreach ($arrJs as $js)
	        <script src="{{ $js }}"></script>
	    @endforeach
    @endisset
    @yield('scripts')
</body>
</html>
