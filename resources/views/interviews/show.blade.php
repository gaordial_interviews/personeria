@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-primary">Ver entrevista</h2>
		</div>
	</div>
	<div class="row">
        <div class="col-md-3">
            <label for="" class="font-weight-bold label-info">ID:</label>
        </div>
        <div class="col-md-3">
            <label for="">{{ $interview->id }}</label>
        </div>
        <div class="col-md-3">
            <label for="" class="font-weight-bold label-info">Estado:</label>
        </div>
        <div class="col-md-3">
            <label for="">{{ $interview->nombre_estado }}</label>
        </div>
    </div>
    <div class="row">
		<div class="col-md-3">
			<label for="" class="font-weight-bold label-info">Nombres:</label>
		</div>
		<div class="col-md-3">
			<label for="">{{ $interview->nombres }}</label>
		</div>
		<div class="col-md-3">
			<label for="" class="font-weight-bold label-info">Apellidos:</label>
		</div>
		<div class="col-md-3">
			<label for="">{{ $interview->apellidos }}</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<label for="" class="font-weight-bold label-info">Identificación:</label>
		</div>
		<div class="col-md-3">
			<label for="">{{ $interview->identificacion }}</label>
        </div>
        <div class="col-md-3">
            <label for="" class="font-weight-bold label-info">Fecha nacimiento:</label>
        </div>
        <div class="col-md-3">
            <label for="">{{ $interview->fec_nacimiento }}</label>
        </div>
    </div>
    <div class="row">
		<div class="col-md-3">
			<label for="" class="font-weight-bold label-info">Domicilio:</label>
		</div>
		<div class="col-md-3">
			<label for="">{{ $interview->domicilio }}</label>
        </div>
        <div class="col-md-3">
            <label for="" class="font-weight-bold label-info">Ciudad:</label>
        </div>
        <div class="col-md-3">
            <label for="">{{ $interview->ciudad }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for="" class="font-weight-bold label-info">Creado el:</label>
        </div>
        <div class="col-md-9">
            <label for="">{{ $interview->created_at }}</label>
        </div>
    </div>
@endsection
{{-- @section('scripts')
	<script src="{{ asset('js/responsables/index.js') }}"></script>
@endsection --}}
