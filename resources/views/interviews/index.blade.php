@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center text-primary">Lista de entrevistas</h1>
		</div>
    </div>
    <div class="row">
        <div class="col-md-12">&nbsp;</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<table id="activitiesTable" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="min-width: 100px">ID</th>
						<th class="text-center">Nombres</th>
						<th class="text-center">Apellidos</th>
						<th class="text-center">Email</th>
						<th class="text-center">Teléfono</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Opciones</th>
					</tr>
                </thead>
                @if (!empty($interviews))
                    <tbody>
                    @foreach ($interviews as $interview)
                        <tr>
                            <td>{{ $interview->id }}</td>
                            <td>{{ $interview->nombres }}</td>
                            <td>{{ $interview->apellidos }}</td>
                            <td>{{ $interview->fec_nacimiento }}</td>
                            <td>{{ $interview->domicilio }}</td>
                            <td>{{ $interview->nombre_estado }}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route("entrevistas.ver", $interview->id) }}">Ver <i class="fas fa-eye"></i></a>
                                <a class="btn btn-primary" href="{{ route("entrevistas.borrar", $interview->id) }}">Borrar <i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
			</table>
		</div>
	</div>
@endsection
